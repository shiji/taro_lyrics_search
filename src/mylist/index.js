import Taro from '@tarojs/taro'
import { View } from '@tarojs/components'

import PropTypes from 'prop-types'
import classNames from 'classnames'

import AtComponent from '../../common/component'

import './index.scss'

export default class MYAtList extends AtComponent {
  render () {
    const rootClass = classNames(
      'at-list',
      {
        'at-list--no-border': !this.props.hasBorder
      },
      this.props.className
    )

    return <View className={rootClass}>{this.props.children}</View>
  }
}

MYAtList.defaultProps = {
  hasBorder: true
}

MYAtList.propTypes = {
  hasBorder: PropTypes.bool
}
