import Taro, { Component } from '@tarojs/taro'
import PropTypes from 'prop-types'
import { View, Text, Button } from '@tarojs/components'
import _isFunction from 'lodash/isFunction'

import './index.less'
const defaultFunc = () => {}
export default class Lyclistitem extends Component {
  static defaultProps = {
    onAddone: defaultFunc,
  }
  componentDidMount() {
    console.log(this.props)
  }
  onShow(txt,e){
    // console.log(this.props.onAddOneCollect)
    // this.props.onAddone(txt)
    this.props.onAddone(txt)
     
  }

  render() {
    const { datatitle,datacontent,onAddone } = this.props
    
    return (
      
      <View>
        {datacontent.map((item,i) => (
          <View className='list' >
            <View className='list-main'>
              <View className='list-main-right'> 
                <View className='list-item' style='display: flex;'>
                  <view style='flex: 1;'>
                    <view><Text className='content'>{item}</Text></view>
                    <view><Text className='title'>《{datatitle[i]}》</Text></view>
                  </view>
                  <view style=''>
                    <Button className='tickets' onClick={this.onShow.bind(this,i)}>收藏</Button>
                  </view>
                </View>
              </View>
            </View>
          </View>
        ))}
      </View>
    )
  }
}

Lyclistitem.propTypes = {
  data: PropTypes.array,
  onAddone: PropTypes.func.isRequired
}
