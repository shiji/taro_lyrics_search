

import Taro, { Component } from '@tarojs/taro'
import PropTypes from 'prop-types'
import { View, Text, Button } from '@tarojs/components'
import _isFunction from 'lodash/isFunction'

import './index.less'
const defaultFunc = () => {}
export default class LycCollectlistitem extends Component {
  static defaultProps = {
    onDeleteone: defaultFunc,
  }
  
  /**
   * @name: 
   * @msg: 
   * @param {index} 
   * @return: 
   */
  onShow(index,e){
    // console.log(this.props.onAddOneCollect)
    // this.props.onAddone(txt)
    this.props.onDeleteone(index)
    console.log(index)
  }
  
  render() {
    const { datatitle,datacontent } = this.props
    
    return (
      
      <View style='height:500rpx'>
        {datacontent.map((item,i) => (
          <View className='list' key={item} >
            <View className='list-main'>
              <View className='list-main-right'> 
                <View className='list-item' style='display: flex;'>
                  <view style='flex: 1;'>
                    <view><Text className='content'>{item}</Text></view>
                    <view><Text className='title'>《{datatitle[i]}》</Text></view>
                  </view>
                  <view style=''>
                    <Button className='tickets' onClick={this.onShow.bind(this,i)}>删除</Button>
                  </view>
                </View>
              </View>
            </View>
          </View>
        ))}
      </View>
    )
  }
}

LycCollectlistitem.propTypes = {
  data: PropTypes.array,
  
}
